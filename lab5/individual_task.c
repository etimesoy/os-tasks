#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <fcntl.h>
#include <copyfile.h>

// Отсортировать в заданном каталоге (аргумент 1 командной строки)
// и во всех его подкаталогах файлы по следующим критериям
// (аргумент 2 командной строки, задаётся в виде целого числа):
// 1 – по размеру файла, 2 – по имени файла.
// Записать файлы в порядке сортировки в новый каталог (аргумент 3 командной строки).
// После записи каждого файла выводить на консоль полный путь каталога, имя файла, размер файла.

const int FILE_PATH_MAX_LENGTH = 1000;
const int FILE_NAME_MAX_LENGTH = 100;
const int MAX_FILES_COUNT = 100;
const int INDEX_STRING_MAX_LENGTH = 10;

struct File
{
    char path[FILE_PATH_MAX_LENGTH];
    char name[FILE_NAME_MAX_LENGTH];
    int size;
};

int copy_file(const char* source, const char* destination)
{    
    int input = open(source, O_RDONLY), output = creat(destination, 0660);    
    if (input == -1)
    {
        return -1;
    }    
    if (output == -1)
    {
        close(input);
        return -1;
    }

    int result = fcopyfile(input, output, 0, COPYFILE_ALL);

    close(input);
    close(output);

    return result;
}

bool left_size_is_less(struct File file1, struct File file2)
{
    return file1.size < file2.size;
}

bool left_name_is_less(struct File file1, struct File file2)
{
    return strcmp(file1.name, file2.name) < 0;
}

char* appending_slash(char* path)
{
    if (path[strlen(path) - 1] != '/')
    {
        strcat(path, "/");
    }
    return path;
}

bool is_current_or_parent_dir_pointer(char* dir_name)
{
    return strcmp(".", dir_name) == 0 || strcmp("..", dir_name) == 0;
}

int string_to_int(char* string)
{
    return *string - '0';
}

int read_files(char* dir_name, struct File* files, int* files_count)
{
    DIR* dir = opendir(dir_name);
    if (dir == NULL)
    {
        printf("Не получилось открыть директорию %s\n", dir_name);
        return -1;
    }

    struct dirent* dir_item = readdir(dir);
    while (dir_item != NULL)
    {
        if (is_current_or_parent_dir_pointer(dir_item->d_name))
        {
            dir_item = readdir(dir);
            continue;
        }

        char dir_item_path[FILE_PATH_MAX_LENGTH];
        strcpy(dir_item_path, dir_name);
        strcat(dir_item_path, dir_item->d_name);
        struct stat dir_item_stat;
        lstat(dir_item_path, &dir_item_stat);

        if (S_ISDIR(dir_item_stat.st_mode))
        {
            read_files(appending_slash(dir_item_path), files, files_count);
        }
        else if (S_ISREG(dir_item_stat.st_mode))
        {
            struct File file;
            strcpy(file.name, dir_item->d_name);
            strcpy(file.path, dir_name);
            file.size = (int)dir_item_stat.st_size;

            (*files_count)++;
            files[(*files_count) - 1] = file;
        }

        dir_item = readdir(dir);
    }
 
    int dir_close_status = closedir(dir);
    if (dir_close_status != 0)
    {
        printf("Возникла ошибка при закрытии директории %s\n", dir_name);
        return -1;
    }

    return 0;
}

void sort_files(int sort_option, struct File* files, int* files_count)
{
    bool (*should_be_exchanged)(struct File file1, struct File file2);
    if (sort_option == 1)
    {
        should_be_exchanged = left_size_is_less;
    }
    else
    {
        should_be_exchanged = left_name_is_less;
    }

    for (int i = 0; i < (*files_count); i++)
    {
        for (int j = 0; j < (*files_count) - i - 1; j++)
        {
            if (!should_be_exchanged(files[j], files[j + 1]))
            {
                struct File file = files[j];
                files[j] = files[j + 1];
                files[j + 1] = file;
            }
        }
    }
}

int write_files(char* dir_name, struct File* files, int* files_count)
{
    DIR* dir = opendir(dir_name);
    if (dir == NULL)
    {
        printf("Не получилось открыть директорию %s\n", dir_name);
        return -1;
    }

    for (int i = 0; i < (*files_count); i++)
    {
        printf("Путь: %s, ", files[i].path);
        printf("имя: %s, ", files[i].name);
        printf("размер: %d\n", files[i].size);

        char src_file_path[FILE_PATH_MAX_LENGTH];
        strcpy(src_file_path, files[i].path);
        strcat(src_file_path, files[i].name);

        char index_string[INDEX_STRING_MAX_LENGTH];
        sprintf(index_string, " (%d)", i);
        char dest_file_path[FILE_PATH_MAX_LENGTH];
        strcpy(dest_file_path, dir_name);
        strcat(dest_file_path, files[i].name);
        strcat(dest_file_path, index_string);

        if (copy_file(src_file_path, dest_file_path) == -1)
        {
            printf("Возникла ошибка при копировании файла %s\n", dest_file_path);
        }
    }

    int dir_close_status = closedir(dir);
    if (dir_close_status != 0)
    {
        printf("Возникла ошибка при закрытии директории %s\n", dir_name);
        return -1;
    }

    return 0;
}

int main(int argc, char** args)
{
    struct File files[MAX_FILES_COUNT];
    int files_count = 0;

    char source_dir[FILE_PATH_MAX_LENGTH];
    realpath(args[1], source_dir);
    int sort_option = string_to_int(args[2]);
    char dest_dir[FILE_PATH_MAX_LENGTH];
    realpath(args[3], dest_dir);

    if (sort_option != 1 && sort_option != 2)
    {
        printf("Введен неправильный критерий сортировки\n");
        return -1;
    }

    int status_code = read_files(appending_slash(source_dir), files, &files_count);
    if (status_code != 0)
    {
        return status_code;
    }

    sort_files(sort_option, files, &files_count);

    return write_files(appending_slash(dest_dir), files, &files_count);
}
