#include <stdio.h>

// Написать программу вывода содержимого текстового файла на экран
// (в качестве аргумента при запуске программы передаётся имя файла,
//  второй аргумент (lines_number) устанавливает вывод по группам строк (по lines_number строк)
//  или сплошным текстом (lines_number=0)).
// Для вывода очередной группы строк необходимо ожидать нажатия пользователем любой клавиши.
// Для чтения или записи файла использовать только функции посимвольного ввода-вывода
// getc(), putc(), fgetc(), fputc().
// Предусмотреть контроль ошибок открытия/закрытия/чтения/записи файла.

const char NEW_LINE_CHARACTER = '\n';

int string_to_int(char* string)
{
    return *string - '0';
}

void print_file_content(FILE* file_descriptor)
{
    int character = fgetc(file_descriptor);
    while (character != EOF)
    {
        printf("%c", character);
        character = fgetc(file_descriptor);
    }
}

int print_file_content_part(FILE* file_descriptor, int lines_number)
{
    for (int i = 0; i < lines_number; i++)
    {
        int character = fgetc(file_descriptor);
        if (character != EOF)
        {
            while (character != NEW_LINE_CHARACTER)
            {
                printf("%c", character);
                character = fgetc(file_descriptor);
            }
            printf(NEW_LINE_CHARACTER);
        }
        else if (feof(file_descriptor) != 0)
        {
            return 0;
        }
        else
        {
            printf("Возникла ошибка при чтении файла\n");
            return 0;
        }
    }
    return 1;
}

int main(int argc, char** args)
{
    char* file_name = args[1];
    int lines_number = string_to_int(args[2]);

    FILE* file_descriptor = fopen(file_name, "r");
    if (file_descriptor == NULL)
    {
        printf("Не получилось открыть файл на чтение\n");
        return -1;
    }

    if (lines_number == 0)
    {
        print_file_content(file_descriptor);
    }
    else
    {
        int should_continue = print_file_content_part(file_descriptor, lines_number);;
        while (should_continue != 0)
        {
            printf("Нажмите любую клавишу для продолжения: ");
            getchar();
            should_continue = print_file_content_part(file_descriptor, lines_number);
        }
    }

    int close_status = fclose(file_descriptor);
    if (close_status != 0)
    {
        printf("Возникла ошибка при закрытии файла\n");
        return -1;
    }

    return 0;
}
