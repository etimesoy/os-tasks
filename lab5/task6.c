#include <stdio.h>
#include <dirent.h>

// Написать программу вывода на экран содержимого текущего и заданного первым
// параметром вызова программы каталогов.
// Предусмотреть контроль ошибок открытия/закрытия/чтения каталога.

void print_dir_files(DIR* folder) 
{
    struct dirent* element;
    element = readdir(folder);
    if (element == NULL)
    {
        printf("Не получилось прочитать содержимое директории\n");
        return;
    }
    while (element != NULL)
    {
        printf("    %s\n", element->d_name);
        element = readdir(folder);
    }
}

int main(int argc, char** args)
{
    char* folder2_name = args[1];
    DIR* folder1 = opendir(".");
    DIR* folder2 = opendir(folder2_name);
    if (folder1 == NULL)
    {
        printf("Не получилось открыть текущую директорию\n");
        return -1;
    }
    if (folder2 == NULL)
    {
        printf("Не получилось открыть директорию %s\n", folder2_name);
        return -1;
    }

    printf("Текущая директория:\n");
    print_dir_files(folder1);
    printf("Директория %s:\n", folder2_name);
    print_dir_files(folder2);

    int folder1_close_status = closedir(folder1);
    int folder2_close_status = closedir(folder2);
    if (folder1_close_status != 0)
    {
        printf("Возникла ошибка при закрытии текущей директории\n");
        return -1;
    }
    if (folder2_close_status != 0)
    {
        printf("Возникла ошибка при закрытии директории %s\n", folder2_name);
        return -1;
    }

    return 0;
}
