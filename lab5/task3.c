#include <stdio.h>
#include <string.h>

// Написать программу ввода символов с клавиатуры и записи их в файл
// (в качестве аргумента при запуске программы вводится имя файла).
// Для чтения или записи файла использовать только функции
// посимвольного ввода-вывода getc(), putc(), fgetc(), fputc().
// Предусмотреть выход после нажатия комбинации клавиш (например: ctrl-F).
// Предусмотреть контроль ошибок открытия/закрытия/чтения файла.

// Комбинация клавиш Ctrl+F соответствует символу '' или числу 6
const int CTRL_F_CHARACTER = 6;

int main(int argc, char** args)
{
    FILE* file_descriptor = fopen(args[1], "w");
    if (file_descriptor == NULL)
    {
        printf("Не получилось открыть файл на запись\n");
        return -1;
    }

    int character = getc(stdin);
    while (character != CTRL_F_CHARACTER)
    {
        fputc(character, file_descriptor);
        character = getc(stdin);
    }

    int close_status = fclose(file_descriptor);
    if (close_status != 0)
    {
        printf("Возникла ошибка при закрытии файла\n");
        return -1;
    }

    return 0;
}
