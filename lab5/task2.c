#include <stdio.h>

// Написать программу вывода сообщения на экран.

const int INPUT_STRING_MAX_SIZE = 100;

int main()
{
    char message[INPUT_STRING_MAX_SIZE];
    printf("Введите сообщение: ");
    fgets(message, INPUT_STRING_MAX_SIZE, stdin);
    printf("Введенное сообщение: %s", message);
    return 0;
}
