#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>

// Написать программу копирования одного файла в другой.
// В качестве параметров при вызове программы передаются имена первого и второго файлов.
// Для чтения или записи файла использовать только функции посимвольного ввода-вывода
// getc(), putc(), fgetc(), fputc().
// Предусмотреть копирование прав доступа к файлу и контроль ошибок открытия/закрытия/чтения/записи файла.

int main(int argc, char** args)
{
    char* file1_name = args[1];
    char* file2_name = args[2];
    FILE* file1_descriptor = fopen(file1_name, "r");
    FILE* file2_descriptor = fopen(file2_name, "w");

    if (file1_descriptor == NULL)
    {
        printf("Не получилось открыть файл %s на чтение\n", file1_name);
        return -1;
    }
    if (file2_descriptor == NULL)
    {
        printf("Не получилось открыть файл %s на запись\n", file2_name);
        return -1;
    }

    int character = getc(file1_descriptor);
    while (character != EOF)
    {
        fputc(character, file2_descriptor);
        character = getc(file1_descriptor);
    }

    int file1_close_status = fclose(file1_descriptor);
    int file2_close_status = fclose(file2_descriptor);
    if (file1_close_status != 0)
    {
        printf("Возникла ошибка при закрытии файла %s\n", file1_name);
        return -1;
    }
    if (file2_close_status != 0)
    {
        printf("Возникла ошибка при закрытии файла %s\n", file2_name);
        return -1;
    }

    struct stat file1_stat;
    stat(file1_name, &file1_stat);
    chmod(file2_name, file1_stat.st_mode);

    return 0;
}
