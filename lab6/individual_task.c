#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdbool.h>

// Разработать программу «интерпретатор команд», которая воспринимает команды,
// вводимые с клавиатуры, (например, ls -l /bin/bash)
// и осуществляет их корректное выполнение.
// Для этого каждая вводимая команда должна выполняться в отдельном процессе
// с использованием вызова exec(). Предусмотреть контроль ошибок.

const int INPUT_STRING_MAX_SIZE = 200;
const int COMMAND_ARG_MAX_SIZE = 100;

void split(char* string, char** into_strings)
{
    char* delimiters = " \n";
    char* part = strtok(string, delimiters);
    int i;
    for (i = 0; part != NULL; i++)
    {
        into_strings[i] = part;
        part = strtok(NULL, delimiters);
    }
    into_strings[i] = NULL;
}

bool handle_command(char** argv)
{
    pid_t pid = fork();
    if (pid == 0)
    {
        pid_t inner_pid = fork();
        if (inner_pid == 0)
        {
            execvp(argv[0], argv);
            // Если execvp не найдет введеную команду,
            // то продолжится выполнение кода в текущем процессе (он не будет заменен новым)
            printf("Не удалось найти команду: %s\n", argv[0]);
        }
        else if (inner_pid > 0)
        {
            int status;
            waitpid(inner_pid, &status, 0);
            if (WIFEXITED(status))
            {
                int exit_status = WEXITSTATUS(status);
                if (exit_status != 0)
                {
                    printf("Выполнение команды завершилось с кодом %d\n", exit_status);
                }
            }
            else
            {
                printf("Выполнение команды завершилось неожиданно\n");
            }
        }
        else if (inner_pid < 0)
        {
            printf("Ошибка создания процесса для выполнения команды. Пожалуйста, попробуйте еще раз.\n");
        }
        return false;
    }
    else if (pid < 0)
    {
        printf("Ошибка создания процесса для выполнения команды. Пожалуйста, попробуйте еще раз.\n");
    }
    return true;
}

int main()
{
    bool should_continue = true;
    while (should_continue)
    {
        char command[INPUT_STRING_MAX_SIZE];
        // printf("$ ");
        fgets(command, INPUT_STRING_MAX_SIZE, stdin);

        char* argv[COMMAND_ARG_MAX_SIZE];
        split(command, argv);

        should_continue = handle_command(argv);
    }
    return 0;
}
