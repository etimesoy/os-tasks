#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>

// Написать программу, создающую два дочерних процесса с использованием двух вызовов fork().
// Родительский и два дочерних процесса должны
// выводить на экран свой pid и pid родительского процесса и текущее время
// в формате: часы : минуты : секунды : миллисекунды.
// Используя вызов system(), выполнить команду ps -x в родительском процессе.
// Найти свои процессы в списке запущенных процессов.

const int BUFFER_SIZE = 200;

void get_current_time(char* into_string)
{
    time_t now_time;
    time(&now_time);
    struct tm* now_tm = localtime(&now_time);
    struct timespec now_timespec;
    clock_gettime(CLOCK_MONOTONIC, &now_timespec);
    long milliseconds = now_timespec.tv_nsec / 1000000;

    snprintf(into_string, BUFFER_SIZE, "%d:%d:%d:%ld", now_tm->tm_hour, now_tm->tm_min, now_tm->tm_sec, milliseconds);
}

void print_pid_info(pid_t pid)
{
    char time_string[BUFFER_SIZE];
    if (pid == 0)
    {
        get_current_time(time_string);
        printf("Это Дочерний процесс, его pid=%d время: %s\n", getpid(), time_string);
        get_current_time(time_string);
        printf("    pid его Родительского процесса=%d время: %s\n", getppid(), time_string);
    }
    else if (pid > 0)
    {
        get_current_time(time_string);
        printf("Это Родительский процесс pid=%d время: %s\n", getpid(), time_string);
    }
    else
    {
        printf("Ошибка вызова fork, потомок не создан\n");
    }
}

int main()
{
    pid_t pid1 = fork();
    print_pid_info(pid1);

    if (pid1 > 0)
    {
        pid_t pid2 = fork();
        print_pid_info(pid2);
        if (pid2 > 0)
        {
            system("ps -x");
        }
    }

    return 0;
}
